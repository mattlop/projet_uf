DROP IF EXISTS Consommer;
DROP IF EXISTS Reservoir;
DROP IF EXISTS Relationner;
DROP IF EXISTS EquiperTheorique;
DROP IF EXISTS EquiperReel;
DROP IF EXISTS EquipeAfillier;
DROP IF EXISTS Role;
DROP IF EXISTS Jour;
DROP IF EXISTS Utilisateur;
DROP IF EXISTS Equipe;
DROP IF EXISTS Dependre;
DROP IF EXISTS TypeRetard;
DROP IF EXISTS EtapeOF;
DROP IF EXISTS EtapeVP;
DROP IF EXISTS Etape;
DROP IF EXISTS OrdreFabrication;
DROP IF EXISTS VP;
DROP IF EXISTS TypeVP;
DROP IF EXISTS Batiment;
DROP IF EXISTS Site;
DROP IF EXISTS Equipement;


CREATE TABLE Etape(
   etape_id INT IDENTITY,
   etape_nom VARCHAR(50),
   etape_description VARCHAR(255),
   etape_duree_theorique_sec BIGINT,
   etape_callage VARCHAR(10) NOT NULL,
   etape_res_nec_theorique VARCHAR(50) NOT NULL,
   PRIMARY KEY(etape_id)
);

CREATE TABLE Site(
   site_id INT IDENTITY,
   site_nom VARCHAR(50),
   PRIMARY KEY(site_id)
);

CREATE TABLE Utilisateur(
   user_id VARCHAR(255),
   user_login VARCHAR(25),
   user_nom VARCHAR(50),
   user_prenom VARCHAR(50),
   user_admin BIT NOT NULL,
   PRIMARY KEY(user_id)
);

CREATE TABLE Role(
   role_id INT IDENTITY,
   role_nom VARCHAR(50) NOT NULL,
   PRIMARY KEY(role_id)
);

CREATE TABLE Reservoir(
   res_id INT IDENTITY,
   res_nom VARCHAR(50) NOT NULL,
   res_min INT,
   res_max INT,
   res_current INT NOT NULL,
   PRIMARY KEY(res_id)
);

CREATE TABLE Jour(
   jour_id INT IDENTITY,
   jour_date DATE,
   jour_position VARCHAR(3),
   user_id VARCHAR(255) NOT NULL,
   PRIMARY KEY(jour_id),
   FOREIGN KEY(user_id) REFERENCES Utilisateur(user_id)
);

CREATE TABLE TypeVP(
   type_id INT IDENTITY,
   type_libelle CHAR(3) NOT NULL,
   type_desc VARCHAR(150),
   PRIMARY KEY(type_id)
);

CREATE TABLE Batiment(
   bat_id INT IDENTITY,
   bat_nom VARCHAR(50) NOT NULL,
   site_id INT NOT NULL,
   PRIMARY KEY(bat_id),
   FOREIGN KEY(site_id) REFERENCES Site(site_id)
);

CREATE TABLE Equipement(
   equ_id INT IDENTITY,
   equ_nom VARCHAR(50),
   bat_id INT NOT NULL,
   PRIMARY KEY(equ_id),
   FOREIGN KEY(bat_id) REFERENCES Batiment(bat_id)
);

CREATE TABLE Equipe(
   equi_id INT IDENTITY,
   equi_nom VARCHAR(50),
   equi_roulette VARCHAR(50) NOT NULL,
   equi_roulette_debut DATE NOT NULL,
   bat_id INT NOT NULL,
   PRIMARY KEY(equi_id),
   FOREIGN KEY(bat_id) REFERENCES Batiment(bat_id)
);

CREATE TABLE EquipeAfillier(
   user_id VARCHAR(255),
   equi_id INT,
   aff_id INT IDENTITY,
   aff_debut DATETIME2 NOT NULL,
   aff_fin DATETIME2 NOT NULL,
   role_id INT NOT NULL,
   PRIMARY KEY(user_id, equi_id, aff_id),
   FOREIGN KEY(user_id) REFERENCES Utilisateur(user_id),
   FOREIGN KEY(equi_id) REFERENCES Equipe(equi_id),
   FOREIGN KEY(role_id) REFERENCES Role(role_id)
);

CREATE TABLE VP(
   vp_id INT IDENTITY,
   vp_nom VARCHAR(50) NOT NULL,
   vp_nom_sap VARCHAR(50),
   vp_num_article VARCHAR(50) NOT NULL,
   vp_num_alternative VARCHAR(50),
   type_id INT NOT NULL,
   bat_id INT NOT NULL,
   PRIMARY KEY(vp_id),
   FOREIGN KEY(type_id) REFERENCES TypeVP(type_id),
   FOREIGN KEY(bat_id) REFERENCES Batiment(bat_id)
);

CREATE TABLE EtapeVP(
   etape_id INT,
   etape_ordre TINYINT,
   vp_id INT NOT NULL,
   PRIMARY KEY(etape_id),
   FOREIGN KEY(etape_id) REFERENCES Etape(etape_id),
   FOREIGN KEY(vp_id) REFERENCES VP(vp_id)
);

CREATE TABLE OrdreFabrication(
   of_id INT IDENTITY,
   of_nom VARCHAR(50) NOT NULL,
   of_quantite INT,
   of_unite VARCHAR(50),
   of_debut_plannifie DATETIME2,
   of_numlot VARCHAR(50),
   vp_id INT,
   PRIMARY KEY(of_id),
   FOREIGN KEY(vp_id) REFERENCES VP(vp_id)
);

CREATE TABLE EtapeOF(
   etape_id INT,
   etape_debut_theorique DATETIME2 NOT NULL,
   etape_debut_reel DATETIME2 NOT NULL,
   etape_duree_reel_sec BIGINT NOT NULL,
   etape_etat TINYINT NOT NULL,
   etape_nom_bis VARCHAR(50),
   etape_res_nec_reel VARCHAR(50),
   of_id INT NOT NULL,
   PRIMARY KEY(etape_id, of_id),
   FOREIGN KEY(etape_id) REFERENCES Etape(etape_id),
   FOREIGN KEY(of_id) REFERENCES OrdreFabrication(of_id)
);

CREATE TABLE TypeRetard(
   tr_id INT IDENTITY,
   tr_libelle VARCHAR(100) NOT NULL,
   tr_decallage_sec BIGINT NOT NULL,
   tr_avance BIT NOT NULL,
   tr_recule BIT NOT NULL,
   tr_diminuer BIT NOT NULL,
   tr_allonge BIT NOT NULL,
   etape_id INT NOT NULL,
   tr_id_1 INT,
   PRIMARY KEY(tr_id),
   FOREIGN KEY(etape_id) REFERENCES EtapeOF(etape_id),
   FOREIGN KEY(tr_id_1) REFERENCES TypeRetard(tr_id)
);

CREATE TABLE Relationner(
   etape_id INT,
   etape_id_1 INT,
   duree_decallage_sec BIGINT,
   type_callage TINYINT NOT NULL,
   asap BIT NOT NULL,
   PRIMARY KEY(etape_id, etape_id_1),
   FOREIGN KEY(etape_id) REFERENCES Etape(etape_id),
   FOREIGN KEY(etape_id_1) REFERENCES Etape(etape_id)
);

CREATE TABLE EquiperTheorique(
   etape_id INT,
   equ_id INT,
   ordre SMALLINT,
   priorite SMALLINT,
   PRIMARY KEY(etape_id, equ_id),
   FOREIGN KEY(etape_id) REFERENCES EtapeVP(etape_id),
   FOREIGN KEY(equ_id) REFERENCES Equipement(equ_id)
);

CREATE TABLE Dependre(
   of_id INT,
   of_id_1 INT,
   PRIMARY KEY(of_id, of_id_1),
   FOREIGN KEY(of_id) REFERENCES OrdreFabrication(of_id),
   FOREIGN KEY(of_id_1) REFERENCES OrdreFabrication(of_id)
);

CREATE TABLE Consommer(
   etape_id INT,
   res_id INT,
   Quantite INT,
   PRIMARY KEY(etape_id, res_id),
   FOREIGN KEY(etape_id) REFERENCES Etape(etape_id),
   FOREIGN KEY(res_id) REFERENCES Reservoir(res_id)
);

CREATE TABLE EquiperReel(
   equ_id INT,
   etape_id INT,
   PRIMARY KEY(equ_id, etape_id),
   FOREIGN KEY(equ_id) REFERENCES Equipement(equ_id),
   FOREIGN KEY(etape_id) REFERENCES EtapeOF(etape_id)
);
