**Sommaire**

> Pour afficher les images dans le Markdown, il est conseiller de télécharger le .md avec le répertoire `imgMD`.

- [1. Lancer l'application](#1-lancer-lapplication)
- [2. Structure algorithmique](#2-structure-algorithmique)
  - [2.1. Organisation du projets](#21-organisation-du-projets)
    - [2.1.1. SocketLibrary](#211-socketlibrary)
    - [2.1.2. MainControlService](#212-maincontrolservice)
    - [2.1.3. DCM_Object](#213-dcm_object)
    - [2.1.4. OSIRIS](#214-osiris)
- [3. Organisation de l'application OSIRIS](#3-organisation-de-lapplication-osiris)
  - [3.1. Les différentes pages](#31-les-différentes-pages)
    - [3.1.1. Page de test](#311-page-de-test)
    - [3.1.2. Page des opérateurs](#312-page-des-opérateurs)
    - [3.1.3. Page du planning](#313-page-du-planning)
    - [3.1.4. Page des recettes](#314-page-des-recettes)
    - [3.1.5. Page des Ordre de Fabrication](#315-page-des-ordre-de-fabrication)
  - [3.2. Les différentes vues](#32-les-différentes-vues)
    - [3.2.1. Les inputs de dates](#321-les-inputs-de-dates)
    - [3.2.2. Les vues pour la fabrication](#322-les-vues-pour-la-fabrication)
    - [3.2.3. Les vues équipes et opérateurs](#323-les-vues-équipes-et-opérateurs)
    - [3.2.4. Les vues des recettes et équipement](#324-les-vues-des-recettes-et-équipement)
    - [3.2.5. Les vues de la plannification](#325-les-vues-de-la-plannification)
    - [3.2.6. Autres ressources](#326-autres-ressources)

# 1. Lancer l'application

> Etant donné que l'application fonctionne avec des services, le server, l'API et l'application cliente sera tous les trois lancée sur la machine.

Pour lancer la solution localement pour test : 
1. Assurez vous de posséder une base de donnée SQLServer sinon l'installer [ici](https://www.microsoft.com/fr-fr/sql-server/sql-server-downloads?rtc=1) (*prendre la version **Express***). Lancer le script SQL d'installation de la base pour préparer l'environnement. Le fichier SQL : `script_install.sql`
2. Sur la racine du projet, lancer l'exécutable `Server/bin/Debug/Server.exe`.
3. Puis lancer l'API se trouvant dans `APITest/bin/Debug/APITest.exe`.
4. Vous devriez avoir maintenant 2 fenêtres consoles avec ces informations : 
![](imgMD/launching_solution.png)
5. Lancer l'application client se situant dans ce répertoire : `STIG/bin/Debug/OSIRIS.exe`. Le menu devrait charger en quelques secondes.

# 2. Structure algorithmique 

## 2.1. Organisation du projets

![project directory](imgMD/directory_project.png)

- `SocketLibrary` : bibliothèque du projet servant à la communication des applications. Il utilise `Socket.IO`.
- `DCM_Object` : bibliothèque d'objet pour le projet
- `MainControlService` : bibliothèque pour le service de contrôle principal (*le serveur*)
- `ServeurTEST` : utilise la bibliothèque `MainControlService` en mode console pour simuler le serveur localement. (*utilisé pour les testes*)
- `APIService` : bibliothèque pour l'API Client lourd
- `APITest` : utilise la bibliothèque `APIService` en mode console pour simuler l'API localement. (*utilisé pour les testes*)
- `ClientTEST` : utilise la classe `SocketLibrary.ClientSocket.cs`, sert uniquement à tester les fonction client.
- `OSIRIS` : L'application lourde cliente.
- `IniApp` : petite librairie personnel pour gérer les fichiers de configurations

### 2.1.1. SocketLibrary

> Cette bibliothèque à pour but une meilleur utilisation de la librairie `Socket.IO` et de adapter au besoin du projet. La gestion de la communication est gérer automatiquement et/ou paramétrable.

Contient plusieurs éléments : 
- `ClientSocket` : à utiliser comme instance pour se connecter à une socket serveur
- `ServerSocket` : à utiliser comme instance pour accueillir des clients sockets
- `StateObject` : première couche de classe pour les messages envoyées entre le client et le serveur. Les message sont en `byte[]`.
- `MessageSocket` : deuxième couche de classe pour gérer les messages entre le client et le serveur. Normalisation du contenu des messages.

### 2.1.2. MainControlService

Composés de : 
- `MainService` : Le cœur principale du service
- `SQL_Communication` : Classe de communication avec la Base de donnée. Elle est responsable de l'extraction de la BDD au lancement du serveur

### 2.1.3. DCM_Object

Chaque objet en base de donnée est convertie en objet C#. Chaque objet est du type `MObject` et possède tous un identifiant objet.

La classe `ModelUpdatedEngine` gère les données des objets DCM.

La classe `MessageEngine` est la troisième couche créer spécialement pour les objets DCM. Cela permet d'implémenter le typage des messages, de connaitre le ou les destinataires et de connaître l'émetteur.

### 2.1.4. OSIRIS

> L'application principale du client.

Organisation de ce projet : 
- `Images` : les images du projets
- `Pages` : les pages du projets 
- `Views` : toutes les vues et interfaces pour afficher les données
- `DataCommunication` : classe intermédiaire entre `ModelUpdatedEngine` et les vues. Gère les modifications de l'utilisateur ainsi que la requête de donnée. Permet aussi de trier les données pour ensuite l'envoyer au vues.
- `MainWindows` : La fenêtre principale qui accueillera les pages.

# 3. Organisation de l'application OSIRIS

Quand l'application démarre, c'est l'objet `MainWindow` qui est appelé en premier. C'est ici que sera gérer l'instanciation des pages.

Tout d'abord, l'instance de `MainWindow` créer un objet `DataCommunication` (*qui sera l'unique instance créer pour cette objet.*) et essaie de se connecter à l'API : `DataCommunication.TryConnection()`. Une fois la connexion établie, l'instance va demander à l'API (par le biais de la classe `SocketLibrary.ClientSocket`) de récupérer les données : `DataCommunication.LoadData()`.

Une fois les données chargées, la fenêtre affiche par défaut le menu principale `SwitchPage(PAGE.MENU)`. 

Cette fonction va créer une instance de la page sélectionner et l'afficher sur la fenêtre.

!["page folder"](imgMD/page_folder.png)

!["all pages"](imgMD/menu_osiris.png)

## 3.1. Les différentes pages 

### 3.1.1. Page de test
(*ne serra pas présente dans la version final de l'application*)

![](imgMD/page_test.png)

> Permet de vérifier graphiquement les vues tel que les boutons, les checkbox ect.

### 3.1.2. Page des opérateurs

Listes des équipes avec leurs roulette :
![](imgMD/page_operator.png)

Listes des affiliations avec l'emploie du temps de chaqu'un (modifiable) :
![](imgMD/page_team.png)

> C'est sur cette interface que sera gérer les affiliations des utilisateurs au équipes, la gestion des équipes et des roulettes de chaqu'une. Une interface de modification des roulettes en cours est disponible.

Cette page utilise les [vues des opérateurs et équipements](#323-les-vues-équipes-et-opérateurs) ainsi que les [inputs des dates](#321-les-inputs-de-dates)

### 3.1.3. Page du planning

![](imgMD/page_planning.png)

> Affichage du planning sous plusieurs vues possibles. Avec l'affichage des ressources disponibles (*nombres d'opérateurs présent*) et des ressources nécessaire par tâches.

[les vues utilisées](#325-les-vues-de-la-plannification) 

### 3.1.4. Page des recettes

Page des recettes :
![](imgMD/page_recette.png)

Les étapes d'une recette : 
![](imgMD/page_data_recette.png)

> Menu des recette de produit, avec la listes des étapes nécessaire avec leur contrainte de ressources et de machine.

Utilise les vues [recettes et les équipement](#324-les-vues-des-recettes-et-équipement) ainsi que les [ressources](#326-autres-ressources)

### 3.1.5. Page des Ordre de Fabrication

![](imgMD/page_manifacturing.png)

> Interface de lancement des ordres de fabrication à partir des recettes.

la page utilise [les vues pour la fabrication](#322-les-vues-pour-la-fabrication) ainsi que pour la [planification](#325-les-vues-de-la-plannification)

## 3.2. Les différentes vues

> Situer dans le répertoire `.../STIG/Views`

### 3.2.1. Les inputs de dates

> répertoire : `.../STIG/Views/DateAndTime`

`MLO_DatePicker` : Interface pour sélectionner une date (utilise l'objet `Popup` pour s'afficher)

![](imgMD/MLO_DateTimePicker.png)

---

`MLO_DateTimeProperty` : inputs pour accueillir les donnée de type `DateTime`. 
- Pour les modification des dates : utilise `MLO_DatePicker`
- Pour les modification des heures : utilise `MLO_HourPicker`

![](imgMD/MLO_DateTimeProperty.png)

---

`MLO_Duration` : inputs pour accueillir une propriété de durée définit par un double.

- Pour passer en mode édition, appeler la fonction `Edit()`

![](imgMD/MLO_Duration.png)

---

`MLO_HourPicker` : input pour sélectionner une heure (utilise l'objet `Popup` pour s'afficher)

> Pour afficher plusieurs segments, la classe `PartGrowingShart` est instancier plusieurs fois. Une instance représente une heure ou une minutes dans l'horloge.

![](imgMD/MLO_HourPicker.png)

---

`Popup` : fenêtre popup personnalisable 

Voici un exemple de code pour créer une fenêtre et récupérer les informations :
``` C#
# l'élément input 
TextBox element = new TextBox();
element.Name = "monTB"

Popup window = new Popup(true);
# Ajoute l'élément dans la fenêtre avec un présentateur d'élément.
window.AddContent(element, true, true, "Mon textbox")

# Si l'utilisateur à cliquer sur 'Valider'
if(window.ShowDialog() == true){
    # Récupérer les informations de l'élément
    string monTexte = ((TextBox)window.results["monTB"]).Text;
}
```

### 3.2.2. Les vues pour la fabrication

> répertoire : `.../STIG/Views/Fabrication`

---

`DataRecetteOF` : tableaux d'élément pour afficher les recettes pour lancer les OF. Chaque lignes est une instance de `ContentRecetteOF`.

![](imgMD/DataRecetteOF.png)

---

`ContentRecetteOF` : une ligne représentant une recette avec plusieurs informations sur la recette

![](imgMD/ContentRecetteOF.png)

### 3.2.3. Les vues équipes et opérateurs

> répertoire : `.../STIG/Views/Operateur`

---

`Calendrier` : vue qui affiche le calendrier d'un objet. Pour le faire fonctionner il faut l'initialiser avec `Init(DataCommunication dc)` et selectionner un objet à représenter `Selected(MObject parent)`.

Voici les types d'objets supporter : 
- `MBatiment` : affiche la roulette des équipes
- `MEquipe` : affiche l'emploie du temps des utilisateurs affiliée aujourd'hui. (*l'interface de modification est disponible pour ce type de vue*)

![](imgMD/Calendrier.png)

---

`Calendrier_Item` : représente une ligne du timeline de l'objet `Calendrier`.

![](imgMD/Calendrier_Item.png)

---

`Calendrier_Case` : représente une case de la ligne de l'objet `Calendrier_Item`.

![](imgMD/Calendrier_Case.png)

---

`ViewEquipe` : button représentant une équipe, affiche certaine donnée de base comme le nombre d'utilisateur affilier à l'équipe et le nombre de présent selon leur emploie du temps.

Une fois cliquer, celui appel une instance de `ViewEquipeDetail`

![](imgMD/ViewEquipe.png)

---

`ViewEquipeDetail` : petite fenêtre donnant les détail d'une équipe. Les affiliations et l'emploie du temps de chaque utilisateurs affiliés. 

![](imgMD/ViewEquipeDetail.png)

---

`VED_User` : vue représentant une affiliation dans la fenêtre `ViewEquipeDetail`. Il est possible de modifier l'affiliation ou de le supprimer.

Quand le boutton de modification est cliquer, l'objet fait appel à une instance de `Popup` pour afficher une fenêtre de modification.

![](imgMD/VED_User.png)

### 3.2.4. Les vues des recettes et équipement

> répertoire : `.../STIG/Views/Recette` et `.../STIG/Views/Recette/Equipement`

---

`DataRecette` : tableaux représentant la liste des recettes

Pour s'afficher, celui-ci doit être initialiser `Init(DataCommunication, RecipePage)`. Il faut ensuite lui spécifier un batiment `SelectBatiment(MBatiment)`

Chaque recette créer une instance de `ContentRecette`

![](imgMD/DataRecette.png)

---

`ContentRecette` : ligne représentant une recette

![](imgMD/ContentRecette.png)

---

`DataEtape` : tableaux représentant la liste des étapes d'une recette

![](imgMD/DataEtape.png)

---

`DataEtapeRow` : ligne représentant une étape d'une recette

![](imgMD/DataEtapeRow.png)

---

`ViewEtape` : interface qui affiche la prévisualisation des étapes d'une recette

Pour s'afficher, celui-ci doit être initialiser `Init(DataCommunication dc, DataRecipePage drp, long vp_id)`.

![](imgMD/ViewEtape.png)

---

`ViewEtapeItem` : la vue compris dans `ViewEtape` définissant une étape. Selon la durer de celle-ci, la vue est plus grande.

![](imgMD/ViewEtapeItem.png)

---

`ViewEtapeLien` : l'affichage du lien des étapes

---

`EquipementView` : La fenêtre pour afficher les affiliations des équipement au tâche.
Cette interface fonctionne par simple glisser déposer.

![](imgMD/EquipementView.png)

---

`EquipementItem` : Vue pour un équipement, n'affiche que son nom

- S'il est déjà affilier à la tâche elle s'affiche en vert, sinon en bleu

![](imgMD/EquipementItem.png)

---

`EquipementLinks` : Vue qui donne une affiliation, avec plusieurs alternative d'utilisation d'équipement représenter par les instances de `EquipementLinkItem`. 

- Chaque `EquipementLinks` est une affiliation **nécessaire** pour l'étape
- Chaque `EquipementLinkItem` dans `EquipementLinks` est une affiliation **possible** pour l'étape.

![](imgMD/EquipementLinks.png)


### 3.2.5. Les vues de la plannification

> répertoire : `.../STIG/Views/Plannification/Gant` et `.../STIG/Views/Plannification/Gant/GantItems`

---

`DragDropManager`

> *Classe pas encore construite. Elle servira à l'optimisation de l'organisation du drag and drop*

---

`ViewGant` : Interface gérant un affichage des objet dans le temps. Selon le type de vue donnée (`TYPE_VIEW`), l'affichage des étapes seront reparties différemment. 

![](imgMD/ViewGant.png)

---

`ItemDetails` : Vue flottante pour afficher les détails d'un objet

![](imgMD/ItemDetails.png)

---

`DateName` : outil pour convertir en string les dates ou gérer les échelles de temps plus facilement. (utiliser par `Gant_TimeLine`)

---

`IGantItem` : interface global pour les objet de vue appartenant à `ViewGant`.

---

`Gant_Item` : représente une étape 

![](imgMD/Gant_Item.png)

---

`Gant_Lien` : représente un lien entre deux étapes 

---

`Gant_Section` : ligne horizontal prenant toute la longueur d'une ligne pour définir une section

![](imgMD/Gant_Section.png)

---

`Gant_Horizontal_Desc` : vue qui donne la description de la ligne (*exemple détermine les étapes d'une machine*)

![](imgMD/Gant_Horizontal_Desc.png)

---

`Gant_TimeLine` : interface gérant et donnant l'échelle de temps. Cette échelle peut-être modifier à souhait par l'utilisateur.

- Un drag and drop déplacera la timeline
- Un mouvement de la molette de la souris modifiera l'échelle de temps

![](imgMD/Gant_TimeLine.png)

---

`Gant_Ressources` : Interface calculant les ressources actuelles et nécessaire sous forme de graph.

![](imgMD/Gant_Ressources.png)

---

`Gant_Ressource_Item` : Objet de graph permettant d'afficher les ressources dans `Gant_Ressources`.


### 3.2.6. Autres ressources

> répertoire : `.../STIG/Views/Other` 

---

`Ressources` : input pour accueillir une propriété string (*représentant les ressources d'une étape par exemple*)

Pour passer en mode édition il faut appeler la fonction `Edit()`. 

Pour modifier en détail les ressources, l'instance fait appel à une instance de `RessourcesPicker`

``` C#
#exemple de ressources en format string
string res = "1,1,1,0.5,3,3"
```

![](imgMD/Ressources.png)

---

`RessourcesPicker` : vue permettant de modifier en détail d'une propriété ressource

Appel une instance de `Popup` pour s'afficher

![](imgMD/RessourcesPicker.png)

---

> répertoire : `.../STIG/Views` 

`ButtonLoading` (*pas encore utilisé*)

---

`ButtonMenu` : button de navigation entre les pages. Appeler par la page `MenuPage`

![](imgMD/buttonMenu.png)

---

`ButtonPlanning` (*pas encore fonctionnel*)

---

`ButtonReturn` : boutton de retour en arrière. Gérer et définit par la fenêtre principale (`MainWindow`)

![](imgMD/ButtonReturn.png)

---

`MLO_ComboBox` (*pas encore fonctionnel*)

---

`ColorsTools` : *classe statique* gérant la couleur de l'application. Possède plusieurs fonctions pour modifier les couleurs et les conversions.
